package me.sherafgan.itunes_crawler;

import me.sherafgan.itunes_crawler.parse.PageDocument;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.IOException;

public class PageDocumentFile implements PageDocument {
    private final File file;
    private final String text;
    private final String url;

    public PageDocumentFile(File file, String text, String url) {
        this.file = file;
        this.text = text;
        this.url = url;
    }

    public PageDocumentFile(File file, String text) {
        this.file = file;
        this.text = text;
        this.url = "LOCAL_HTML_PAGE_FOR_TEST";
    }

    @Override
    public Document get() {
        Document document = null;
        try {
            document = Jsoup.parse(file, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return document;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public String getUrl() {
        return url;
    }
}
