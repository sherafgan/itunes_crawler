package me.sherafgan.itunes_crawler.parse.impl;

import me.sherafgan.itunes_crawler.PageDocumentFile;
import me.sherafgan.itunes_crawler.parse.CategoryLink;
import me.sherafgan.itunes_crawler.parse.LetterLink;
import me.sherafgan.itunes_crawler.parse.PageDocument;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CategoryLinkImplTest {
    CategoryLink categoryLink;
    Set<String> categoriesSet = new HashSet<>(Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
            "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "#"));

    @BeforeEach
    void setUp() throws URISyntaxException {
        File categoryPageSource =
                new File(Paths.get(getClass().getResource("/" + "BooksCategoryPage.html").toURI()).toString());
        PageDocument pageDocument = new PageDocumentFile(categoryPageSource, PageType.CATEGORY.toString());
        categoryLink = new CategoryLinkImpl(pageDocument);
    }

    @Test
    void parseCategoryLink_successful() {
        List<LetterLink> letterLinks = categoryLink.parse();
        assertEquals(letterLinks.size(), categoriesSet.size());
        List<String> letters = new ArrayList<>();
        letterLinks.forEach(letterLink -> {
            letters.add(letterLink.getText());
        });
        assertTrue(categoriesSet.containsAll(letters));
    }
}