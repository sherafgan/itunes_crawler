package me.sherafgan.itunes_crawler.parse.impl;

import me.sherafgan.itunes_crawler.PageDocumentFile;
import me.sherafgan.itunes_crawler.parse.AppStorePage;
import me.sherafgan.itunes_crawler.parse.CategoryLink;
import me.sherafgan.itunes_crawler.parse.PageDocument;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class AppStorePageImplTest {
    AppStorePage appStorePage;
    Set<String> categoriesSet = new HashSet<>(Arrays.asList("Books", "Business", "Catalogs", "Education",
            "Entertainment", "Finance", "Food & Drink", "Games", "Health & Fitness", "Lifestyle",
            "Magazines & Newspapers", "Medical", "Music", "Navigation", "News", "Photo & Video", "Productivity",
            "Reference", "Shopping", "Social Networking", "Sports", "Stickers", "Travel", "Utilities", "Weather"));

    @BeforeEach
    void setUp() throws URISyntaxException {
        File appStorePageSource =
                new File(Paths.get(getClass().getResource("/" + "AppStoreItunesPage.html").toURI()).toString());
        PageDocument pageDocument = new PageDocumentFile(appStorePageSource, PageType.MAIN_PAGE.toString());
        appStorePage = new AppStorePageImpl(pageDocument);
    }

    @Test
    void parseAppStorePage_successful() {
        List<String> categoriesParsed = new ArrayList<>();
        List<CategoryLink> categoryLinks = appStorePage.parse();
        for (CategoryLink categoryLink : categoryLinks) {
            categoriesParsed.add(categoryLink.getText());
            assertNotNull(categoryLink.getUrl());
            assertFalse(categoryLink.getUrl().trim().isEmpty());
        }
        assertEquals(categoriesSet.size(), categoriesParsed.size());
        assertTrue(categoriesSet.containsAll(categoriesParsed));
    }
}