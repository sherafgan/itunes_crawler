package me.sherafgan.itunes_crawler.parse.impl;

import me.sherafgan.itunes_crawler.PageDocumentFile;
import me.sherafgan.itunes_crawler.parse.AppLink;
import me.sherafgan.itunes_crawler.parse.LetterLink;
import me.sherafgan.itunes_crawler.parse.PageDocument;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LetterAndPageLinksImplTest {
    LetterLink letterLink;
    Set<String> categoriesSet = new HashSet<>(Arrays.asList("X is for Xavier", "X Minus One - Old Time Radio App",
            "X-D", "X-O Manowar #1", "X.E. Ecology", "Xatirə Fərəcli - Şeirlər", "XBook-亦动亦静的阅读方式", "XBQDND",
            "XCEM", "Xchange App", "Xcode Interactive Tutorials for Xcode8 and Swift3", "Xcode5互动教程 for Object-C 精典版",
            "Xcode 11 and Swift 5 Tutorials", "XellissimeBook", "Xem Boi 2017 - Boi Bai - Tu Vi Dinh Dau - Tu Vi",
            "Xem Bói", "Xem hướng nhà", "Xenodohio Atlantis, parakalo", "Xeo", "Xeo English", "XEZI: Story Mode",
            "Xhosa Bible", "XI ABCP", "Xixón de Novela", "Xmas Santa Hd Frames - PicShop", "Xmas Special Puzzle Game",
            "XMI SmartLifestyle Presentation", "XMLittreDict", "XPS Reader Pro", "Xtend", "XtremeCare",
            "Xu Phat Giao Thong - GPLX", "XVI B-MRS Meeting", "XY BT CONTROLL", "X小说", "X美院"));

    @BeforeEach
    void setUp() throws URISyntaxException {
        File file =
                new File(Paths.get(getClass().getResource("/" + "BooksCategoryXPage0.html").toURI()).toString());
        PageDocument pageDocument = new PageDocumentFile(file, PageType.LETTER.toString(),
                "https://apps.apple.com/us/genre/ios-books/id6018?letter=X");
        letterLink = new LetterLinkImpl(pageDocument);
    }

    @Test
    void parseCategoryLetterPage_successful() {
        List<AppLink> appLinks = letterLink.parse();
        assertEquals(categoriesSet.size(), appLinks.size());
        List<String> appNames = new ArrayList<>();
        appLinks.forEach(appLink -> {
            appNames.add(appLink.getText());
        });
        categoriesSet.containsAll(appNames);
    }
}