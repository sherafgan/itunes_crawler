package me.sherafgan.itunes_crawler.data;

import me.sherafgan.itunes_crawler.data.domain.AppLinkDAO;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AppLinkRepository extends MongoRepository<AppLinkDAO, Long> {
}
