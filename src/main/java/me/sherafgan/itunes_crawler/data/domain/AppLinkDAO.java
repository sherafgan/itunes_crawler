package me.sherafgan.itunes_crawler.data.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import me.sherafgan.itunes_crawler.parse.AppLink;
import org.springframework.data.annotation.Id;


@AllArgsConstructor
@NoArgsConstructor
@Getter
public class AppLinkDAO {
    @Id
    private Long id;
    private String text;
    private String url;

    public AppLinkDAO(AppLink appLink) {
        this.id = appLink.getId();
        this.text = appLink.getText();
        this.url = appLink.getUrl();
    }
}
