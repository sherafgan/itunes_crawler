package me.sherafgan.itunes_crawler.data.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class StoreAppDAO {
    @Id
    private Long trackId;

    private String description;
    private String bundleId;
    private String artistViewUrl;
    private String artworkUrl60;
    private String artworkUrl100;
    private List<String> appletvScreenshotUrls;
    private List<String> screenshotUrls;
    private List<String> ipadScreenshotUrls;
    private String artworkUrl512;
    private Boolean isGameCenterEnabled;
    private List<String> supportedDevices;
    private List<String> advisories;
    private String kind;
    private List<String> features;
    private String trackCensoredName;
    private String trackViewUrl;
    private String contentAdvisoryRating;
    private List<String> languageCodesISO2A;
    private String fileSizeBytes;
    private Float averageUserRatingForCurrentVersion;
    private Long userRatingCountForCurrentVersion;
    private String trackContentRating;
    private Integer primaryGenreId;
    private String sellerName;
    private String sellerUrl;
    private String minimumOsVersion;
    private String trackName;
    private String currentVersionReleaseDate;
    private String releaseNotes;
    private Boolean isVppDeviceBasedLicensingEnabled;
    private String primaryGenreName;
    private List<String> genreIds;
    private String releaseDate;
    private String formattedPrice;
    private String currency;
    private String version;
    private String wrapperType;
    private Long artistId;
    private String artistName;
    private List<String> genres;
    private Float price;
    private Float averageUserRating;
    private Long userRatingCount;
}
