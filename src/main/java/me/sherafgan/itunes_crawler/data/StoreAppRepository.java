package me.sherafgan.itunes_crawler.data;

import me.sherafgan.itunes_crawler.data.domain.StoreAppDAO;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StoreAppRepository extends MongoRepository<StoreAppDAO, Long> {
}
