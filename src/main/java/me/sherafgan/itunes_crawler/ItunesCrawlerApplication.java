package me.sherafgan.itunes_crawler;

import lombok.extern.slf4j.Slf4j;
import me.sherafgan.itunes_crawler.service.ParallelAppStoreCrawler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class ItunesCrawlerApplication implements CommandLineRunner {
    @Autowired
    ParallelAppStoreCrawler parallelAppStoreCrawler;

    public static void main(String[] args) {
        SpringApplication.run(ItunesCrawlerApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        parallelAppStoreCrawler.crawl();
    }
}
