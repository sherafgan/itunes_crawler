package me.sherafgan.itunes_crawler.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import me.sherafgan.itunes_crawler.data.domain.StoreAppDAO;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class AppInfoDTO {
    private Integer resultCount;
    private List<StoreAppDAO> results;
}
