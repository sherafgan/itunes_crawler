package me.sherafgan.itunes_crawler.parse;

import java.util.List;

public interface CategoryLink {
    public List<LetterLink> parse();

    public String getText();

    public String getUrl();
}
