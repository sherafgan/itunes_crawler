package me.sherafgan.itunes_crawler.parse;

import java.util.List;

public interface PageLink {
    public List<AppLink> parse();
}
