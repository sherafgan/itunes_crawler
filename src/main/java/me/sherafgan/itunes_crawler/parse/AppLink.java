package me.sherafgan.itunes_crawler.parse;

import me.sherafgan.itunes_crawler.data.domain.StoreAppDAO;

public interface AppLink {
    StoreAppDAO parse();

    Long getId();

    String getText();

    String getUrl();
}
