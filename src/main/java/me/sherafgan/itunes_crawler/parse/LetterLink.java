package me.sherafgan.itunes_crawler.parse;

import java.util.List;

public interface LetterLink {
    List<AppLink> parse();

    public String getText();
}
