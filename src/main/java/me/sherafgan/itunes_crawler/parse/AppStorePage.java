package me.sherafgan.itunes_crawler.parse;

import java.util.List;

public interface AppStorePage {
    public List<CategoryLink> parse();
}
