package me.sherafgan.itunes_crawler.parse.impl;

import lombok.extern.slf4j.Slf4j;
import me.sherafgan.itunes_crawler.parse.AppLink;
import me.sherafgan.itunes_crawler.parse.PageDocument;
import me.sherafgan.itunes_crawler.parse.PageLink;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static me.sherafgan.itunes_crawler.service.ParallelAppStoreCrawler.totalNumberOfAppLinksGathered;

@Slf4j
public class PageLinkImpl implements PageLink {
    private static final String[] CATEGORY_PAGE_COLUMN_NAMES = new String[]{"column first", "column", "column last"};
    private final PageDocument pageDocument;

    public PageLinkImpl(PageDocument pageDocument) {
        this.pageDocument = pageDocument;
    }

    @Override
    public List<AppLink> parse() {
        Document document = pageDocument.get();
        if (document != null) {
            return parsePage(document);
        }
        return Collections.emptyList();
    }

    private List<AppLink> parsePage(Document page) {
        List<AppLink> appLinks = new ArrayList<>();
        for (String columnName : CATEGORY_PAGE_COLUMN_NAMES) {
            Element columnList = page.body()
                    .getElementsByClass(columnName).first()
                    .getElementsByTag("ul").first();
            appLinks.addAll(parseColumnItemsOf(columnList));
        }
        return appLinks;
    }

    private List<AppLink> parseColumnItemsOf(Element columnList) {
        List<AppLink> appLinks = new ArrayList<>();
        for (Element columnItem : columnList.children()) {
            Element appTag = columnItem.getElementsByTag("a").first();
            String[] appURLSplit = appTag.attr("href").split("/");
            String appId = appURLSplit[appURLSplit.length - 1].substring(2);
            AppLink appLink = new AppLinkImpl(Long.valueOf(appId), appTag.ownText(), appTag.attr("href"));
            appLinks.add(appLink);
            totalNumberOfAppLinksGathered.incrementAndGet();
        }
        return appLinks;
    }
}
