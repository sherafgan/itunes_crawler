package me.sherafgan.itunes_crawler.parse.impl;

public enum PageType {
    MAIN_PAGE,
    CATEGORY,
    LETTER,
    LETTER_NUMBER_PAGE
}
