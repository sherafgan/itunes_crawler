package me.sherafgan.itunes_crawler.parse.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import me.sherafgan.itunes_crawler.data.domain.AppLinkDAO;
import me.sherafgan.itunes_crawler.data.domain.StoreAppDAO;
import me.sherafgan.itunes_crawler.dto.AppInfoDTO;
import me.sherafgan.itunes_crawler.parse.AppLink;
import org.jsoup.Jsoup;

import java.io.IOException;

@Slf4j
@Getter
public class AppLinkImpl implements AppLink {
    private static final String GET_APP_INFO_URL = "https://itunes.apple.com/lookup?country=ru&id=";
    private final Long id;
    private final String text;
    private final String url;
    private final ObjectMapper objectMapper;

    public AppLinkImpl(Long id, String text, String url) {
        this.objectMapper = new ObjectMapper();
        this.id = id;
        this.text = text;
        this.url = url;
    }

    public AppLinkImpl(AppLinkDAO appLinkDAO) {
        this.objectMapper = new ObjectMapper();
        this.id = appLinkDAO.getId();
        this.text = appLinkDAO.getText();
        this.url = appLinkDAO.getUrl();
    }

    @Override
    public StoreAppDAO parse() {
        try {
            String responseBody = Jsoup.connect(GET_APP_INFO_URL + id).ignoreContentType(true).execute().body();
            try {
                AppInfoDTO appInfoDTO = objectMapper.readValue(responseBody, AppInfoDTO.class);
                if (appInfoDTO.getResults().size() == 1) {
                    return appInfoDTO.getResults().get(0);
                }
                log.warn(String.format("NO APP INFO IN RESPONSE BODY OF : id%d", id));
            } catch (JsonProcessingException e) {
                log.error(String.format("COULDN'T DESERIALIZE JSON : \"%s\"", responseBody), e);
            }
            return null;
        } catch (IOException e) {
            log.error(String.format("COULDN'T FETCH APP (%s) INFO with URL : %s %n", text, url), e);
        }
        return null;
    }
}
