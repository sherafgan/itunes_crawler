package me.sherafgan.itunes_crawler.parse.impl;

import lombok.extern.slf4j.Slf4j;
import me.sherafgan.itunes_crawler.parse.AppStorePage;
import me.sherafgan.itunes_crawler.parse.CategoryLink;
import me.sherafgan.itunes_crawler.parse.PageDocument;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class AppStorePageImpl implements AppStorePage {
    private static final String[] APP_STORE_PAGE_COLUMN_NAMES = new String[]{"list column first", "list column"};

    private final PageDocument pageDocument;

    public AppStorePageImpl(PageDocument pageDocument) {
        this.pageDocument = pageDocument;
    }

    @Override
    public List<CategoryLink> parse() {
        List<CategoryLink> categoryLinks = new ArrayList<>();
        Document appsStorePage = pageDocument.get();
        if (appsStorePage != null) {
            for (String columnName : APP_STORE_PAGE_COLUMN_NAMES) {
                Element categoryItems = appsStorePage.body().getElementsByClass(columnName).first();
                for (Element categoryItem : categoryItems.children()) {
                    Element categoryTag = categoryItem.getElementsByTag("a").first();
                    categoryLinks.add(new CategoryLinkImpl(new PageDocumentConnection(
                            PageType.CATEGORY, categoryTag.ownText(), categoryTag.attr("href"))));
                }
            }
        }
        return categoryLinks;
    }
}
