package me.sherafgan.itunes_crawler.parse.impl;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import me.sherafgan.itunes_crawler.parse.CategoryLink;
import me.sherafgan.itunes_crawler.parse.LetterLink;
import me.sherafgan.itunes_crawler.parse.PageDocument;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Getter
public class CategoryLinkImpl implements CategoryLink {
    private PageDocument pageDocument;

    public CategoryLinkImpl(PageDocument pageDocument) {
        this.pageDocument = pageDocument;
    }

    @Override
    public List<LetterLink> parse() {
        List<LetterLink> letterLinks = new ArrayList<>();
        Document document = pageDocument.get();
        if (document != null) {
            Element lettersList = document.getElementsByClass("list alpha").first();
            for (Element letterItem : lettersList.children()) {
                Element letterTag = letterItem.getElementsByTag("a").first();
                LetterLink letterLink = new LetterLinkImpl(
                        new PageDocumentConnection(PageType.LETTER, letterTag.ownText(), letterTag.attr("href")));
                letterLinks.add(letterLink);
            }
        }
        return letterLinks;
    }

    @Override
    public String getText() {
        return pageDocument.getText();
    }

    @Override
    public String getUrl() {
        return pageDocument.getUrl();
    }
}
