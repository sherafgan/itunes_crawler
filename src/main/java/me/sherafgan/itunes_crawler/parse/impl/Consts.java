package me.sherafgan.itunes_crawler.parse.impl;

public class Consts {
    public static final Integer TIMEOUT_MULTIPLIER = 3;

    private Consts() {
    }
}
