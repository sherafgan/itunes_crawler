package me.sherafgan.itunes_crawler.parse.impl;

import lombok.extern.slf4j.Slf4j;
import me.sherafgan.itunes_crawler.parse.PageDocument;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.SocketTimeoutException;

@Slf4j
public class PageDocumentConnection implements PageDocument {
    private final String url;
    private final PageType pageType;
    private final String text;

    public PageDocumentConnection(PageType pageType, String text, String url) {
        this.pageType = pageType;
        this.text = text;
        this.url = url;
    }

    public Document get() {
        Document pageDocument = null;
        Connection pageConnection = Jsoup.connect(url);
        try {
            pageDocument = pageConnection.get();
        } catch (SocketTimeoutException socketTimeoutEx) {
            pageConnection.timeout(pageConnection.request().timeout() * Consts.TIMEOUT_MULTIPLIER);
            try {
                pageDocument = pageConnection.get();
            } catch (IOException e) {
                log.error(String.format("COULDN'T PARSE %s w/ bigger timeout(%s): %s with URL : %s %n",
                        pageType, Consts.TIMEOUT_MULTIPLIER, text, url), e);
            }
        } catch (IOException e) {
            log.error(String.format("COULDN'T PARSE %s : %s with URL : %s  %n", pageType.toString(), text, url), e);
        }
        return pageDocument;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public String getUrl() {
        return url;
    }
}
