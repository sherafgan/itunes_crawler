package me.sherafgan.itunes_crawler.parse.impl;

import lombok.extern.slf4j.Slf4j;
import me.sherafgan.itunes_crawler.parse.AppLink;
import me.sherafgan.itunes_crawler.parse.LetterLink;
import me.sherafgan.itunes_crawler.parse.PageDocument;
import me.sherafgan.itunes_crawler.parse.PageLink;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static me.sherafgan.itunes_crawler.service.ParallelAppStoreCrawler.totalNumberOfAppLinksGathered;

@Slf4j
public class LetterLinkImpl implements LetterLink {
    private final PageDocument pageDocument;

    public LetterLinkImpl(PageDocument pageDocument) {
        this.pageDocument = pageDocument;
    }

    @Override
    public List<AppLink> parse() {
        Document document = pageDocument.get();
        if (document != null) {
            return  parseLetterPage(document);
        }
        return Collections.emptyList();
    }

    @Override
    public String getText() {
        return pageDocument.getText();
    }

    private List<AppLink> parseLetterPage(Document categoryLetterPage) {
        Element paginationList = categoryLetterPage.getElementsByClass("list paginate").first();
        if (paginationList != null) {
            return parseGivenPagesAndOrNext(paginationList);
        } else {
            return addPagesAppLinks(pageDocument.getText(), pageDocument.getUrl());
        }
    }

    private List<AppLink> parseGivenPagesAndOrNext(Element paginationList) {
        List<AppLink> appLinks = new ArrayList<>();
        Element nextPageTag = paginationList.getElementsByClass("paginate-more").first();
        if (nextPageTag != null) {
            int pageNumber = 1;
            Element currentPageTag = paginationList.child(pageNumber - 1).getElementsByTag("a").first();
            appLinks.addAll(addPagesAppLinks(currentPageTag.ownText(), currentPageTag.attr("href")));
            while (nextPageTag != null) {
                String nextPageUrl = nextPageTag.attr("href");
                pageNumber++;
                appLinks.addAll(addPagesAppLinks(String.valueOf(pageNumber), nextPageUrl));
                try {
                    Document nextPageParsed = Jsoup.connect(nextPageUrl).get();
                    nextPageTag = nextPageParsed
                            .getElementsByClass("list paginate").first()
                            .getElementsByClass("paginate-more").first();
                } catch (IOException e) {
                    log.warn(String.format("COULDN'T PARSE LETTER-PAGE : %s-%s with URL : %s",
                            pageDocument.getText(), pageNumber, nextPageUrl), e);
                }
            }
        } else {
            for (Element paginationItem : paginationList.children()) {
                Element pageTag = paginationItem.getElementsByTag("a").first();
                appLinks.addAll(addPagesAppLinks(pageTag.ownText(), pageTag.attr("href")));
            }
        }
        return appLinks;
    }

    private List<AppLink> addPagesAppLinks(String pageNumber, String url) {
        log.info(String.format("PARSING LETTER : %s PAGE : %s [%s]", pageDocument.getText(), pageNumber, totalNumberOfAppLinksGathered));
        PageLink pageLink = new PageLinkImpl(new PageDocumentConnection(PageType.LETTER_NUMBER_PAGE, pageNumber, url));
        return pageLink.parse();
    }
}
