package me.sherafgan.itunes_crawler.parse;

import org.jsoup.nodes.Document;

public interface PageDocument {
    public Document get();

    public String getText();

    public String getUrl();
}
