package me.sherafgan.itunes_crawler.service;

import lombok.extern.slf4j.Slf4j;
import me.sherafgan.itunes_crawler.data.AppLinkRepository;
import me.sherafgan.itunes_crawler.data.StoreAppRepository;
import me.sherafgan.itunes_crawler.data.domain.AppLinkDAO;
import me.sherafgan.itunes_crawler.data.domain.StoreAppDAO;
import me.sherafgan.itunes_crawler.parse.AppLink;
import me.sherafgan.itunes_crawler.parse.AppStorePage;
import me.sherafgan.itunes_crawler.parse.CategoryLink;
import me.sherafgan.itunes_crawler.parse.impl.AppLinkImpl;
import me.sherafgan.itunes_crawler.parse.impl.AppStorePageImpl;
import me.sherafgan.itunes_crawler.parse.impl.PageDocumentConnection;
import me.sherafgan.itunes_crawler.parse.impl.PageType;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@Service
public class ParallelAppStoreCrawler {
    private static final String APP_STORE_PAGE_URL = "https://itunes.apple.com/us/genre/ios/id36";
    public static AtomicInteger totalNumberOfAppLinksGathered = new AtomicInteger(0);
    private final StoreAppRepository storeAppRepository;
    private final AppLinkRepository appLinkRepository;

    public ParallelAppStoreCrawler(StoreAppRepository storeAppRepository, AppLinkRepository appLinkRepository) {
        this.storeAppRepository = storeAppRepository;
        this.appLinkRepository = appLinkRepository;
    }

    public void crawl() {
        List<AppLinkDAO> storedApps = appLinkRepository.findAll();
        List<AppLink> parsedApps = new ArrayList<>();
        AppStorePage appStorePage = new AppStorePageImpl(
                new PageDocumentConnection(PageType.MAIN_PAGE, PageType.MAIN_PAGE.toString(), APP_STORE_PAGE_URL));
        List<CategoryLink> categories = appStorePage.parse();
        categories
                .parallelStream()
                .forEach(categoryLink -> categoryLink
                        .parse()
                        .parallelStream()
                        .forEach(letterLink -> {
                            List<AppLink> appLinks = letterLink.parse();
                            parsedApps.addAll(appLinks);
                        }));
        parsedApps
                .parallelStream()
                .forEach(appLink -> {
                    log.info(String.format("DB SAVING APP LINK : %s [%d]", appLink.getId(),
                            totalNumberOfAppLinksGathered.decrementAndGet()));
                    appLinkRepository.save(new AppLinkDAO(appLink));
                });
        totalNumberOfAppLinksGathered.set(storedApps.size());
        storedApps
                .parallelStream()
                .forEach(storedApp -> {
                    AppLink appLink = new AppLinkImpl(storedApp);
                    StoreAppDAO storeAppDAO = appLink.parse();
                    if (storeAppDAO != null) {
                        log.info(String.format("DB SAVING APP INFO : %s [%d]", storeAppDAO.getTrackId(),
                                totalNumberOfAppLinksGathered.decrementAndGet()));
                        storeAppRepository.save(storeAppDAO);
                    }

                });
    }
}
